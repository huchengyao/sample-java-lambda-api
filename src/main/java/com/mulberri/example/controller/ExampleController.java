package com.mulberri.example.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.mulberri.example.dto.ExampleDTO;
import com.mulberri.example.service.ExampleService;

@RestController
@EnableWebMvc
public class ExampleController {

	private ExampleService exampleService = new ExampleService();

	@RequestMapping(path = "/sample/example", method = RequestMethod.GET)
	public ResponseEntity<ExampleDTO> example(@RequestParam("name") String name) {
		ExampleDTO dto = exampleService.getExample(name);
		return ResponseEntity.ok(dto);
	}

	@RequestMapping(path = "/sample/example", method = RequestMethod.POST)
	public ResponseEntity<String> saveExample(@RequestBody ExampleDTO dto) {
		exampleService.saveExample(dto);
		return ResponseEntity.ok("");
	}
}