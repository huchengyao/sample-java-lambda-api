package com.mulberri.example.dynamodb.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "java-example")
public class ExampleDynamodbEntity {
	private Integer age;
	private String name;

	public ExampleDynamodbEntity() {
		// Default constructor is required by AWS DynamoDB SDK
	}

	public ExampleDynamodbEntity(String name, Integer age) {
		this.name = name;
		this.age = age;
	}

	@DynamoDBAttribute
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public void setName(String name) {
		this.name = name;
	}

	@DynamoDBHashKey
	public String getName() {
		return name;
	}
}
